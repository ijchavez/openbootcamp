class Vehiculo():
    color = ""
    ruedas = None
    puertas = None

    def __init__(self, un_color, cant_ruedas, cant_puertas):
        self.color = un_color
        self.ruedas = cant_ruedas
        self.puertas = cant_puertas


class Coche(Vehiculo):
    _vehiculo = None
    _velocidad = None
    _cilindrada = None

    def __init__(self, una_velocidad, cant_cilindraras, un_color, cant_ruedas, cant_puertas):
        super().__init__(un_color, cant_ruedas, cant_puertas)
        self._velocidad = una_velocidad
        self._cilindrada = cant_cilindraras


coche = Coche(150, 8, "rojo", 4, 3)

print("Color: ", coche.color)
print("Ruedas: ", coche.ruedas)
print("Puertas: ", coche.puertas)
print("Velocidad: ", coche._velocidad)
print("Cilindrada: ", coche._cilindrada)


