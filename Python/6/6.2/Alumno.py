class Alumno:
    _nombre = ""
    _nota = None

    def __init__(self, nombre_alumno, nota_alumno):
        self._nombre = nombre_alumno
        self._nota = nota_alumno

    def nombre(self):
        return self._nombre

    def nota(self):
        return self._nota

    def ha_aprobado(self):
        mensaje = ""
        if self.nota() >= 4:
            mensaje = "el alumno ha aprobado"

        else:
            mensaje = "el alumno no ha aprobado"

        return mensaje

    def __str__(self):
        cadena = "Nombre: " + self._nombre + ", Nota: " + str(self._nota) + ", " + self.ha_aprobado()
        return cadena


alumno = Alumno("Gerardo", 1)
print(alumno.__str__())
