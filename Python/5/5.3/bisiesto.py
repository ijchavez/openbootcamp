def anio_bisiesto(anio):
    if(anio % 4 == 0):
        if(anio % 100 != 0):
            if (anio % 400 != 0):
                print(anio, "es divisible por 4", "no es divisible por 100", "no es divisible por 400")
                print(anio, "es bisiesto")

        else:
            if (anio % 400 == 0):
                print(anio, "es divisible por 4", "es divisible por 100", "es divisible por 400")
                print(anio, "es bisiesto")

            else:
                print(anio, "es divisible por 4", "es divisible por 100", "no es divisible por 400")
                print(anio, "no es bisiesto")

    else:
        print(anio, "no es divisible por 4")
        print(anio, "no es bisiesto")
