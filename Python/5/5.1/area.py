import math


def area_triangulo(base, altura):
    area = (base * altura) / 2
    return area


triangulo = area_triangulo(14, 88)
print(triangulo)

pi = math.pi


def area_circulo(radio):
    area = pi * (radio ** 2)
    return area


circulo = area_circulo(154)
print(circulo)
